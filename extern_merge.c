#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#pragma pack(1)
typedef struct chars
{
	char c_name[39];
	int team;
	int level;
	int c_id;
	int guild_id;
} chars;

typedef struct guilds
{
	int guild_id;
	char g_name[49];
} guilds;


/* Reading the original files and breaking them up into sorted intial runs of page_size record runs.
 * Function first reads the first line to determine the file type. Then, it reads file line-by-line, 
 * tokenising them as apporpriate for the file type and writing to a temporary struct. The record 
 * is then inserted into the temporary page. Once the page is full, sort the page using a selection
 * sort then write the ordered page to a binary file. Once the end of file is reached, sort and write
 * and remaining records then return the total number of runs.*/
int read_unsorted_file(char * filename, int page_size, int buffers_no, int file_no, char * file_type, int * rec_count);

/* Loading pages from the runs into the buffer, and calling on the run creation function.
 * Function first readings in bufers_no - 1 pages into the buffer, or until the EOF (whatever comes first). 
 * After reading each page into memory, if either; 
 * 	End of Buffer is reached
 * 	Final run is reached
 * 	End of file is reached
 * Begin the process of merging all current runs in the buffer into a single new run. Each time a page
 * is emptied in the merging process and that run has not reached the end, load the next page in place
 * of the now empty buffer place. Keep doing this until all runs in the buffer are merged. Repeat this
 * process until the end of the file is reached. The function creates one new file per call (pass), which
 * all new runs are written to*/
int merge_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size, char file_type, int list_file_no, int rec_count);

/* Merging pages into a single run in a new binary file. 
 * Function iterates through whole buffer, until it finds the smallest current guild_id. This record is then
 * written to the new binary file. Repeat for next smallest record. If while iterating through the buffer a
 * page is found to have all records written out (and the run that page belongs to has not been entirely read),
 * then stop reading the buffer and return that buffer place number to load in a new page. Repeat until the
 * buffers are all empty and all pages of all runs have been read, then return -2 (End of merge).*/
int create_run(char * output_file, void *** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write, int * page_count, int run_size, char file_type);

int main(int argc, char * argv[])
{
	int page_size, buffers_no, total_1_runs, new_1_runs;
	int file_1_counter = 1, run_1_counter = 1;
	int rec_1_tot = 0;
	int * file_1_no, * run_1_size, * rec_1_ptr;
	char * end_ptr, * file_1_type;
	char file_1_read;
	char end_f_name[50];
	char old_output[50];
	char * extension = ".bin";
	char old_number[3];
	FILE * final_in, * final_out;
	struct chars * temp_char = calloc(1, sizeof(struct chars));
	struct guilds * temp_guild = calloc(1, sizeof(struct guilds));
		
	/* Argument checking */		
	if(argc != 4)
	{
		fprintf(stderr, "Inappropriate number of arguments given.\n");
		printf("%d\n", argc);
		return 1;
	}
		page_size = (int) strtol(argv[1], &end_ptr, 10);
		buffers_no = (int) strtol(argv[2], &end_ptr, 10);
	printf("page_size: %d\n buffers_no: %d\n", page_size, buffers_no);
	run_1_size = &run_1_counter;
	file_1_type = &file_1_read;

	rec_1_ptr = &rec_1_tot;

	/* Reading the original files and writing initial runs to a binary file */
	total_1_runs = read_unsorted_file(argv[3], page_size, buffers_no, 1, file_1_type, rec_1_ptr);
		if(total_1_runs == -1)
			return 1;
	file_1_no = &file_1_counter;

	/* External Merge Sort until both files are sorted */
	while(total_1_runs > 1)
 	{	
		printf("--------------\n");
		if(total_1_runs > 1)
			new_1_runs = merge_pages(file_1_no, page_size, buffers_no, total_1_runs, run_1_size, *file_1_type, 1, *rec_1_ptr);
		if(new_1_runs == -1)
		{
			fprintf(stderr, "program exiting...\n");
			return 1;
		}
		printf("--------------\n");	
  		total_1_runs = new_1_runs;
		printf("--------------\n");
  	}
	
	printf("total_runs: %d\n", total_1_runs);

	/*Get name of final sorted temp file*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", (*file_1_no) - 1);
	strcat(old_output, old_number);
	strcat(old_output, "_1");
	strcat(old_output, extension);
	
	if(*file_1_type == 'c')
		strcpy(end_f_name, "characters.sorted");
	else
		strcpy(end_f_name, "guilds.sorted");

	/* Open final binary file and output file for processing */
	final_in = fopen(old_output, "r+b");
	if(!final_in)
	{
		fprintf(stderr, "Error opening final temp file.\n");
		return 1;
	}


	final_out = fopen(end_f_name, "w+");
	if(!final_out)
	{
		fprintf(stderr, "Error opening sorted file.\n");
		return 1;
	}
	/* write sorted binary file to sorted file */
	if(*file_1_type == 'c')
	{
		while(fread(temp_char, sizeof(struct chars), 1, final_in) == 1)
		{
			fprintf(final_out, "%s,%d,%d,%d,%d\n", temp_char->c_name, temp_char->team, temp_char->level,
				temp_char->c_id, temp_char->guild_id);
		}
	}
	else
	{
		while(fread(temp_guild, sizeof(struct guilds), 1, final_in) == 1)
		{
			fprintf(final_out, "%d,%s\n", temp_guild->guild_id, temp_guild->g_name);
		}
	}	
	/*remove temporary binary file */
	fclose(final_in);
	fclose(final_out);
	remove(old_output);

	free(temp_char);
	free(temp_guild);
	return 0;

}

int read_unsorted_file(char * filename, int page_size, int buffers_no, int file_no, char * file_type, int *rec_count)
{
	int total_pages = 0;
	FILE * f_in;
	FILE * f_out;	
	void * temp_struct, * inter_struct, ** temp_page;
	char * temp_line;
	char * line_token;
	char * temp_name;
	char * temp_team;
	char * temp_level;
	char * temp_c_id;
	char * temp_guild_id;
	char * temp_g_name;
	char * end_ptr;
	char new_file[20] = "temp_0_";
	char new_file_no[3];
	char new_file_type;
	int current_page = 0, i, max, tot_recs = 0;
	unsigned int t_team, t_level, t_c_id, t_guild_id, test;
	printf("page size: %d\n", page_size);	
	f_in = fopen(filename, "r");
	if(f_in == NULL)
	{
		fprintf(stderr, "File open failure.\n");
		return -1;
	}
	temp_line = (char *) calloc(100, sizeof(char));
	
	/* Test for file type */
	fgets(temp_line, 56, f_in);
	line_token = strtok(temp_line, ",");
	test = (int) strtol(line_token, &end_ptr, 10);
	if(test == 0)
	{
		new_file_type = 'c';
		*file_type = 'c';
		temp_page = calloc(page_size, sizeof(struct chars));
		/*inter_struct = calloc(1, sizeof(struct chars));*/
	}
	else
	{
		new_file_type = 'g';
		*file_type = 'g';
		temp_page = calloc(page_size, sizeof(struct guilds));
		/*inter_struct = calloc(1, sizeof(struct guilds));*/
	}
	rewind(f_in);
	sprintf(new_file_no, "%d", file_no);
	strcat(new_file, new_file_no);
	strcat(new_file, ".bin");

	
	f_out = fopen(new_file, "w+b");
	if(f_out == NULL)
	{
		fprintf(stderr, "Output file open failure.\n");
		return -1;
	}

	/* Reading each line of data from the file */
	while(fgets(temp_line, 56, f_in) != NULL)
	{
		if(temp_line == NULL)
			continue;
		if(new_file_type == 'c')
			temp_struct = calloc(1, sizeof(struct chars));
		else
			temp_struct = calloc(1, sizeof(struct guilds));
		temp_line[strlen(temp_line) - 1] = '\0';

		/* Tokenising the line to the required elements */
		line_token = strtok(temp_line, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading 1st token.\n");
			return -1;
		}
		if(new_file_type == 'c')
		{
			temp_name = (char *) calloc(49, sizeof(char));
			strcpy(temp_name, line_token);
		}
		else
		{
			temp_guild_id = (char *)calloc(1, strlen(line_token)+1);
			strcpy(temp_guild_id, line_token);
		}
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading 2nd token.\n");
			return -1;
		}
		if(new_file_type == 'g')
		{
			temp_g_name = (char *) calloc(1, strlen(line_token+1));
			strcpy(temp_g_name, line_token);
		}
		else
		{
			temp_team = (char *) calloc(1, strlen(line_token)+1);
			strcpy(temp_team, line_token);
		}
		line_token = strtok(NULL, ",");
		if(line_token == NULL && new_file_type == 'c')
		{
			fprintf(stderr, "Error reading 3rd token.\n");
			return -1;
		}
		else if(line_token != NULL && new_file_type == 'g')
		{
			fprintf(stderr, "Too many tokens read in guild line.\n");
			return -1;
		}
		if(new_file_type == 'c')
		{	
			temp_level = (char *) calloc(1, strlen(line_token)+1);
			strcpy(temp_level, line_token);
			line_token = strtok(NULL, ",");
			if(line_token == NULL)
			{
				fprintf(stderr, "Error reading 4th token.\n");
				return -1;
			}
			temp_c_id = (char *) calloc(1, strlen(line_token)+1);
			strcpy(temp_c_id, line_token);
			line_token = strtok(NULL, ",");
			if(line_token == NULL)
			{
				fprintf(stderr, "Error reading 5th token.\n");
				return -1;
			}
			temp_guild_id = (char *) calloc(1, strlen(line_token)+1);
			strcpy(temp_guild_id, line_token);
			line_token = strtok(NULL, ",");
			if(line_token != NULL)
			{
				fprintf(stderr, "Error reading 6th token.\n");
				return -1;
			}

			/* Writing elements to temporary record structure */
			t_team = (int) strtol(temp_team, &end_ptr, 10);
			t_level = (int) strtol(temp_level, &end_ptr, 10);
			t_c_id = (int) strtol(temp_c_id, &end_ptr, 10);
			t_guild_id = (int) strtol(temp_guild_id, &end_ptr, 10);
			strcpy(((struct chars *)temp_struct)->c_name, temp_name);
			((struct chars *)temp_struct)->team = t_team;
			((struct chars *)temp_struct)->level = t_level;
			((struct chars *)temp_struct)->c_id = t_c_id;
			((struct chars *)temp_struct)->guild_id = t_guild_id;
		}
		else
		{
			t_guild_id = (int) strtol(temp_guild_id, &end_ptr, 10);
			strcpy(((struct guilds *)temp_struct)->g_name, temp_g_name);
			((struct guilds *)temp_struct)->guild_id = t_guild_id;
		}
		
		/* Add temp record to temp page */
		current_page++;
		tot_recs++;
		temp_page[current_page-1] = temp_struct;
		if(current_page == page_size)
		{
			/* Sorting temp page */
			max = 0;
			total_pages++;
			while(max == 0)
			{
				max = 1;
				/* Finding smallest guild_id */
				for(i = 0; i < page_size-1; i++)
				{
					if(new_file_type == 'c')
					{
						if(((struct chars *)temp_page[i])->guild_id > 
							((struct chars *)temp_page[i+1])->guild_id)
						{
							max = 0;
							inter_struct = temp_page[i];
							temp_page[i] = temp_page[i+1];
							temp_page[i+1] = inter_struct;	
						}
					}
					else
					{
						if(((struct guilds *)temp_page[i])->guild_id > 
							((struct guilds *)temp_page[i+1])->guild_id)
						{
							max = 0;
							inter_struct = temp_page[i];
							temp_page[i] = temp_page[i+1];
							temp_page[i+1] = inter_struct;	
						}	
					}
				}
			} 

			/* Writing page to file*/
			for(i = 0; i < page_size; i++)
			{	
				if(new_file_type == 'c')
				{
					/*printf("CHAR WRITING: %s, %d\n", ((struct chars *)temp_page[i])->c_name, ((struct chars *)temp_page[i])->guild_id);*/
					fwrite(((struct chars *)temp_page[i]), sizeof(struct chars), 1, f_out);
					free(temp_page[i]);
				}
				else
				{
					/*printf("GUILD WRITING: %s, %d\n", ((struct guilds *)temp_page[i])->g_name, ((struct guilds *)temp_page[i])->guild_id);*/
					fwrite(((struct guilds *)temp_page[i]), sizeof(struct guilds), 1, f_out);
					free(temp_page[i]);
				}
			}
			current_page = 0;
		}
		if(new_file_type == 'c')
		{
			free(temp_name);
			free(temp_team);
			free(temp_level);
			free(temp_c_id);
		}
		else
			free(temp_g_name);
		free(temp_guild_id);
		/*free(temp_struct);*/
	}	
		
	/* If there are records remaining in memory that have not been written, sort and write them */
	if(current_page != 0)
	{
		total_pages++;
		max = 0;
		while(max == 0)
		{
			max = 1;
			for(i = 0; i < page_size-1; i++)
			{
				if(new_file_type == 'c')
				{
					if(((struct chars *)temp_page[i])->guild_id > 
						((struct chars *)temp_page[i+1])->guild_id)
					{
						max = 0;
						inter_struct = temp_page[i];
						temp_page[i] = temp_page[i+1];
						temp_page[i+1] = inter_struct;	
					}
				}
				else
				{
					if(((struct guilds *)temp_page[i])->guild_id > 
						((struct guilds *)temp_page[i+1])->guild_id)
					{
						max = 0;
						inter_struct = temp_page[i];
						temp_page[i] = temp_page[i+1];
						temp_page[i+1] = inter_struct;	
					}	
				}
			}
		}
		for(i = 0; i < current_page; i++)
		{
			
			/*printf("Page Record %d: %s, %d\n", i+1, temp_c_page[i]->c_name, temp_c_page[i]->guild_id);
			*/
			if(new_file_type == 'c')
			{
				fwrite(temp_page[i], sizeof(struct chars), 1, f_out);
			}
			else
			{
				fwrite(temp_page[i], sizeof(struct guilds), 1, f_out);
			}
		}
	}
	/**file_type = new_file_type;*/
	if(temp_line != NULL)
		free(temp_line);
	rewind(f_out);
	free(temp_page);	
	fclose(f_in);
	fclose(f_out);
	*rec_count = tot_recs;
	return total_pages;
}


int merge_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size, char file_type, int list_file_no, int rec_count)
{
		
	/* create array for buffer */
	void *** buffer;

	/* position in file for next record in run */
	long int * file_pos = calloc(buffers_no - 1, sizeof(long int));

	/* Number of records that have been read from run */
	int * page_count = calloc(buffers_no - 1, sizeof(int));

	void * temp_struct;
	long int prev = -1;	

	int old_f_no = *file_no - 1;
	char new_output[15];
	char old_output[15];
	char new_number[3];
	char old_number[3];
	char new_f_no[3];
	char * extension = ".bin";
	FILE * f_in, * f_out;
	int i, j, k, eof_flag = 0, end_of_array, buffers_used = 0, run_recs = 0;
	int recs_read = 0, recs_write = 0, new_runs, read_done, write_return, write_complete = 0, move_places;
	
	/*Create new temp file name to write*/
	strcpy(new_output, "temp_");
	sprintf(new_number, "%d", *file_no);
	strcat(new_output, new_number);
	strcat(new_output, "_");
	sprintf(new_f_no, "%d", list_file_no);
	strcat(new_output, new_f_no);
	strcat(new_output, extension);
	/*printf("New file name: %s\n", new_output);*/
	
	/*Get previous temp file to open to read*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", old_f_no);
	strcat(old_output, old_number);
	strcat(old_output, "_");
	strcat(old_output, new_f_no);
	strcat(old_output, extension);
	/*printf("Old file name: %s\n", old_output);*/
	
	/* open old binary file*/
	
	f_in = fopen(old_output, "rb");
	if(!f_in)
	{
		fprintf(stderr, "Error opening old temp file.\n");
		return -1;
	}
	/* set run offsets to 0 */	
	for(i = 0; i < buffers_no - 1; i++)
	{
		page_count[i] = 0;
		file_pos[i] = -1;
	}
	printf("OLD FILE: %s\n", old_output);
	rewind(f_in);
	i = 0;
	j = 0;
	if(file_type == 'c')
	{
		buffer = calloc(buffers_no - 1, sizeof(struct chars));
		buffer[i] = calloc(page_size, sizeof(struct chars));	
		buffer[i][j] = calloc(1, sizeof(struct chars));
		temp_struct = calloc(1, sizeof(struct chars));
	}
	else
	{
		buffer = calloc(buffers_no - 1, sizeof(struct guilds));
		buffer[i] = calloc(page_size, sizeof(struct guilds));	
		buffer[i][j] = calloc(1, sizeof(struct guilds));
		temp_struct = calloc(1, sizeof(struct guilds));
	}	
	new_runs = total_runs;

	/* Reading records into buffers */
	while(1)
	{
		/*printf("%d\n", i);*/
		read_done = 0;
		
		/* Reading a page of records from a run into the ith buffer slot */
		while(j < page_size)
		{
			if(eof_flag == 1)
				break;
			if(file_type == 'c')
			{
				if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
				{	
					eof_flag = 1;
					buffer[i][j] = NULL;
					printf("EOF FOUND\n");
					page_count[i] = (*run_size);
					break;
				}
			}
			else
			{
				if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
				{	
					eof_flag = 1;
					buffer[i][j] = NULL;
					printf("EOF FOUND\n");
					page_count[i] = (*run_size);
					break;
				}
			}
			if(buffer[i][j] == NULL)
				continue;
			read_done = 1;
			recs_read += 1;
			j++;
			if( j < page_size)
			{
				if(file_type == 'c')
					buffer[i][j] = calloc(1, sizeof(struct chars));	
				else
					buffer[i][j] = calloc(1, sizeof(struct guilds));	
			}
		}
		
		/* position of next page to read in */
		file_pos[i] = ftell(f_in);
		
		/* Start read records counter */
		page_count[i] += 1;
	
		/* If the end of file is reached, there are no new runs still to begin being read*/
		if(eof_flag == 1)
		{
			write_complete = 0;
			prev = i;
			printf("EOF ENTERED.\n");
			while(write_complete == 0)
			{
				/* Merge buffer runs*/
				write_return = create_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size, file_type);
				
				/* Error in merging found */
				if(write_return == -1)
					return -1;

				/* All pages in buffer have been read */
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						run_recs = 0;
						write_complete = 1;
				}

				/* Otherwise, a new page of a run needs to be read */
				else
				{
					fseek(f_in, file_pos[write_return], SEEK_SET);
					j = 0;
					i = write_return;
					read_done = 0;
		
					/* Load new page */
					while(j < page_size)
					{
						if(file_type == 'c')
						{
							if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
							{	
								eof_flag = 1;
								buffer[i][j] = NULL;
								printf("EOF FOUND\n");
								page_count[i] = (*run_size);
								break;
							}
						}
						else
						{
							if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
							{	
								eof_flag = 1;
								buffer[i][j] = NULL;
								printf("EOF FOUND\n");
								page_count[i] = (*run_size);
								break;
							}
						}
						if(eof_flag == 1)
							break;
						if(buffer[i][j] == NULL)
							continue;
						read_done = 1;
						/* Increment record counter */
						recs_read += 1;
						j++;
					}	
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
			}
			write_complete = 0;
			break;
		}

		/* If all runs have had a page read into buffer (thus the total run number is less than the buffer size */
		else if(i == (total_runs - 1))
		{
			write_complete = 0;
			prev = i;
			printf("here\n");
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size, file_type);
				if(write_return == -1)
					return -1;
				/* Successful end of run creation */
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;	
						write_complete = 1;
						recs_read = 0;
						run_recs = 0;
				}
				else
				{
					/* Reading new page of a run */
					j = 0;	
					i = write_return;
					fseek(f_in, file_pos[i], SEEK_SET);
					read_done = 0;
					while(j < page_size)
					{
						if(file_type == 'c')
						{
							if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
							{	
								eof_flag = 1;
								buffer[i][j] = NULL;
								printf("EOF FOUND\n");
								page_count[i] = (*run_size);
								break;
							}
						}
						else
						{
							if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
							{	
								eof_flag = 1;
								buffer[i][j] = NULL;
								printf("EOF FOUND\n");
								page_count[i] = (*run_size);
								break;
							}
						}
						if(buffer[i][j] == NULL)
							continue;
						recs_read += 1;
						read_done = 1;
						j++;
					}
					if(eof_flag == 1)
						break;
					page_count[i] += 1;
					file_pos[i] = ftell(f_in);
				}
				if(eof_flag == 1)
					break;
			}
			if(eof_flag == 1)
				continue;
			end_of_array = prev;
			buffers_used += i + 1;
			write_complete = 0;
			eof_flag = 1;				
		}		
		/* If buffers are full or all runs are at the end */
		else if(i == (buffers_no - 2))
		{		
			write_complete = 0;
			prev = i;
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size, file_type);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						write_complete = 1;
						recs_read = 0;
						run_recs = 0;
						fseek(f_in, file_pos[prev], SEEK_SET);
						for(i = 0; i < buffers_no-1; i++)
						{
							file_pos[i] = -1;
							page_count[i] = 0;
							for(j = 0; j < page_size; j++)
								free(buffer[i][j]);
							free(buffer[i]);
						}
				}
				else
				{
					fseek(f_in, file_pos[write_return], SEEK_SET);
					j = 0;
					i = write_return;
					read_done = 0;
					while(j < page_size)
					{
						if(file_type == 'c')
						{
							if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
							{	
								eof_flag = 1;
								buffer[i][j] = NULL;
								printf("EOF FOUND\n");
								page_count[i] = (*run_size);
								break;
							}
						}
						else
						{
							if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
							{	
								eof_flag = 1;
								buffer[i][j] = NULL;
								printf("EOF FOUND\n");
								page_count[i] = (*run_size);
								break;
							}
						}
						if(buffer[i][j] == NULL)
							continue;
						recs_read += 1;
						read_done = 1;
						j++;
					}
					if(eof_flag == 1)
						break;
					page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
			}
			if(eof_flag == 1)
				continue;
			end_of_array = prev;
			buffers_used += prev + 1;
			write_complete = 0;
			i = 0;
			j = 0;
			if(file_type == 'c')
			{
				buffer[i] = calloc(page_size, sizeof(struct chars));
				buffer[i][j] = calloc(1, sizeof(struct chars));
			}
			else
			{
				buffer[i] = calloc(page_size, sizeof(struct guilds));
				buffer[i][j] = calloc(1, sizeof(struct guilds));
			}
			continue;
		}
		/* Calculate how many records to move down to current page of next run */
		move_places = ((*run_size)*page_size)-(page_count[i]*page_size);
		if(move_places > 0)
		{	
			/* Move to next run */
			if(file_type == 'c')
			{
				fseek(f_in, move_places*sizeof(struct chars), SEEK_CUR);		
			}
			else
			{
				fseek(f_in, move_places*sizeof(struct guilds), SEEK_CUR);
			}
		}
		i++;
		j = 0;	
		if(file_type == 'c')
		{	
			buffer[i] = calloc(page_size, sizeof(struct chars));	
			buffer[i][j] = calloc(1, sizeof(struct chars));	
		}
		else
		{
			buffer[i] = calloc(page_size, sizeof(struct guilds));	
			buffer[i][j] = calloc(1, sizeof(struct guilds));	
		}		
	}
	
	*file_no += 1;
	for(k = 0; k <= end_of_array; k++)
	{
		if(page_count[k] < *run_size)
			printf("RUN %d NOT FULLY READ.\n", k);
		for(j = 0; j < page_size; j++)
		{
			free(buffer[k][j]);
		}
		free(buffer[k]);
	}

	i = 0;
	f_out = fopen(new_output, "rb");
	rewind(f_out);
	fclose(f_out);
	free(temp_struct);
	free(file_pos);
	free(page_count);
	fclose(f_in);
	/* Remove old binary file */
	remove(old_output);
	printf("******* RECORDS WRITTEN: %d\n", recs_write);
	printf("Previous run size (in pages): %d\n", *run_size);
	printf("Number of buffers used: %d\n", buffers_used);	
	printf("Previous total runs: %d\n", total_runs);

	/* Calculate new run size */
	if(buffers_used > (buffers_no - 1))
		*run_size = (*run_size) * (buffers_no - 1);
	else
		*run_size = (*run_size) * buffers_used;
	if((*run_size) > (rec_count/page_size))
		*run_size = (int)ceil(((double)(rec_count)/((double)page_size)));
	printf("New run size (in pages): %d\n", *run_size);
	printf("TEST: %.2f\n", ((double)total_runs)/((double)(buffers_no-1)));
	total_runs = (int)ceil(((double)total_runs)/((double)(buffers_no-1)));
	if(total_runs <= 0)
		total_runs = 1;
	printf("New total runs: %d\n", total_runs);	
	printf("NEW FILE: %s\n", new_output);	
	return total_runs;
}

int create_run(char * output_file, void *** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write, int * page_count, int run_size, char file_type)
{
	FILE * f_out;
	int i, j, curr_min = 0, curr_rec = 0, records_flag = 1;
	/* Open new binary file */
	f_out = fopen(output_file, "ab");
	if(!f_out)
	{
		fprintf(stderr, "Error opening new temp file.\n");
		return -1;
	}	
	
	j = 0;
	i = 0;
	
	/* While records are still being written */
	while(records_flag == 1)
	{
		records_flag = 0;
		curr_rec = 0;
		curr_min = 0;
		
		/* Find smallest guild_id */
		for(i = 0; (i < buffers_no - 1 ); i++)
		{
			j = 0;
			/* If end of buffer */
			if( i > end)
				break;	
	
			if(buffer[i][j] == NULL)
				continue;

			/* Finding the current unread record in a page */
			if(file_type == 'c')
			{
				while(strcmp(((struct chars *)buffer[i][j])->c_name, "") == 0)
				{
					j++;
					while(((struct chars *)buffer[i][j]) == NULL && j < page_size)
					{
						j++;
					}
					if(j == page_size)
						break;
				}
			}
			else
			{
				while(strcmp(((struct guilds *)buffer[i][j])->g_name, "") == 0)
				{
					j++;
					while(((struct guilds *)buffer[i][j]) == NULL && j < page_size)
					{
						j++;
					}
					if(j == page_size)
						break;
				}
			}

			/* If the page is empty and it is not the last page of a run
 * 			return the buffer slot number */	
			if(j == page_size)
			{
				if(page_count[i] >= run_size)
					continue;
				else
				{
					fclose(f_out);
					return i;
				}
			}
	
			/* Otherwise a record to write is found */
			records_flag = 1;

			/* check for default min record isn't NULL */
			if(file_type == 'c')
			{
				if(strcmp(((struct chars *)buffer[curr_min][curr_rec])->c_name, "") == 0)
				{
					curr_min = i;
					curr_rec = j;
				}
				/* check if record is new  minimum record */
				else if(((struct chars *)buffer[i][j])->guild_id < ((struct chars *)buffer[curr_min][curr_rec])->guild_id)
				{
					curr_min = i;
					curr_rec = j;
				}
			}
			else
			{
				if(strcmp(((struct guilds *)buffer[curr_min][curr_rec])->g_name, "") == 0)
				{
					curr_min = i;
					curr_rec = j;
				}	
				else if(((struct guilds *)buffer[i][j])->guild_id < ((struct guilds *)buffer[curr_min][curr_rec])->guild_id)
				{
					curr_min = i;
					curr_rec = j;
				}	
			}
		}

		/* If a record has been found to be written out */
		if(records_flag != 0)
		{
			if(file_type == 'c')
			{
				if(fwrite(buffer[curr_min][curr_rec], sizeof(struct chars), 1, f_out) != 1)
				{
					printf("Error writing line!\n");
					fclose(f_out);
					return -1;
				}
			
				/* Set written record position to be null */
				memset(buffer[curr_min][curr_rec], 0, sizeof(struct chars));
			}
			else
			{		
				if(fwrite(buffer[curr_min][curr_rec], sizeof(struct guilds), 1, f_out) != 1)
				{
					printf("Error writing line!\n");
					fclose(f_out);
					return -1;
				}	
				memset(buffer[curr_min][curr_rec], 0, sizeof(struct guilds));

			}
			*run_recs += 1;
			*recs_write += 1;
		}
	}	
	fclose(f_out);
	/* End of merging of runs */
	return -2;
	
}	
