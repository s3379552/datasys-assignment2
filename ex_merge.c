#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#pragma pack(1)
typedef struct chars
{
	char c_name[39];
	int team;
	int level;
	int c_id;
	int guild_id;
} chars;

typedef struct guilds
{
	int guild_id;
	char g_name[49];
} guilds;

/* make seperate function for merge sort */
/* Make array of pages, which is the buffer.
 * Each page is an array of the specified number of structs
 * Merge sort uses buffers_no - 1 buffers as input to merge, 
 * each buffer stores 1 page */

int read_unsorted_character_file(char * filename, int page_size, int buffers_no);
int read_unsorted_guilds_file(char * filename, int page_size, int buffers_no);

int merge_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size);
/* create new output binary file from edited previous filename. Read in original file
 * data by: 
 * 	Create array of buffers_no for pages in memory, another array for file position
 * 	for each buffer.
 * 	Read in struct, record file position using ftell().
 * 	Move (run_size*page_size)-1 places to start of next run, repeat for all buffers.
 *	Start merging. Once all records in a page are done, move to next page.
 *	As a page is merge, write page to new bin file. 
 *	Once all runs in buffers are done, get another buffers_no worth of runs.
 *	Reapeat process until whole input file is done.
 *	p
 *	Return new number of runs.*/

int create_run(char * output_file, struct chars **** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write);

int main(int argc, char * argv[])
{
	int page_size, buffers_no, total_runs, new_pages, new_runs;
	int file_counter = 1, run_counter = 1;
	int * file_no, * run_size;
	char * end_ptr;
	
	char old_output[15];
	char sorted_output[50];
	char old_number[3];
	char * sort_exten = ".sorted";
	char * extension = ".bin";
	FILE * final_in, * final_out;
	struct chars * temp_char = calloc(1, sizeof(struct chars));
	/*Get name of final sorted temp file*/
		
	if(argc != 5)
	{
		fprintf(stderr, "Insufficient number of arguments given.\n");
		return 1;
	}
	page_size = (int) strtol(argv[1], &end_ptr, 10);
	buffers_no = (int) strtol(argv[2], &end_ptr, 10);
	run_size = &run_counter;
	if(strcmp(argv[4], "c") == 0)
	{
		total_runs = read_unsorted_character_file(argv[3], page_size, buffers_no);
		if(total_runs == -1)
			return 1;
	}
	else if(strcmp(argv[4], "g") == 0)
	{
		total_runs = read_unsorted_guilds_file(argv[3], page_size, buffers_no);
		if(total_runs == -1)
			return 1;
	}
	else
	{
		fprintf(stderr, "Please use either the c or g flag.\n");
		return 1;
	}
	file_no = &file_counter;
	while(total_runs > 1)
 	{
		printf("TEST HERE: %d\n", *file_no);
  		new_runs = merge_pages(file_no, page_size, buffers_no, total_runs, run_size);
		if(new_runs == -1)
		{
			fprintf(stderr, "program exiting...\n");
			return 1;
		}
  		total_runs = new_runs;
		printf("--------------\n");
  	}
	
	printf("total_runs: %d\n", total_runs); 
	/*new_runs = merge_pages(file_no, page_size, buffers_no, total_runs, run_size);*/
	
	
	/*Get name of final sorted temp file*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", (*file_no) - 1);
	strcat(old_output, old_number);
	strcat(old_output, extension);
	
	/* Create name of final sorted file */
	strcpy(sorted_output, argv[3]);
	strcat(sorted_output, sort_exten);


	printf("END TEST HERE: %d\n", *file_no);
	printf("SORTED NAME: %s\n", sorted_output);	
	final_in = fopen(old_output, "r+b");
	if(!final_in)
	{
		fprintf(stderr, "Error opening final temp file.\n");
		return 1;
	}


	final_out = fopen(sorted_output, "w+");
	if(!final_out)
	{
		fprintf(stderr, "Error opening sorted file.\n");
		return 1;
	}
	/* write sorted binary file to sorted file */
	while(fread(temp_char, sizeof(struct chars), 1, final_in) == 1)
	{
		/*if(fread(temp_char, sizeof(struct chars), 1, final_in) != 1)
			break;*/
		fprintf(final_out, "%s,%d,%d,%d,%d\n", temp_char->c_name, temp_char->team, temp_char->level,
			temp_char->c_id, temp_char->guild_id);
		/*printf("%s,%d,%d,%d,%d\n", temp_char->c_name, temp_char->team, temp_char->level,
			temp_char->c_id, temp_char->guild_id);*/
	}	
	/*remove temporary binary file */
	fclose(final_in);
	fclose(final_out);
	remove(old_output);
	free(temp_char);
	return 0;
}

int read_unsorted_character_file(char * filename, int page_size, int buffers_no)
{
	int curr_records = 0, total_pages = 0;
	FILE * f_in;
	FILE * f_out;
	struct chars * temp_chars, * inter_char, ** temp_c_page = calloc(page_size, sizeof(struct chars));
	char * temp_line;
	char * line_token;
	char * temp_name;
	char * temp_team;
	char * temp_level;
	char * temp_c_id;
	char * temp_guild_id;
	char * end_ptr;
	char * sent_char = "-1";
	int current_page = 0, i, max;
	unsigned int t_team, t_level, t_c_id, t_guild_id;
	printf("page size: %d\n", page_size);	
	f_in = fopen(filename, "r");
	if(f_in == NULL)
	{
		fprintf(stderr, "Characters file open failure.\n");
		return -1;
	}
	f_out = fopen("temp_0.bin", "w+b");
	if(f_out == NULL)
	{
		fprintf(stderr, "Output file open failure.\n");
		return -1;
	}
	temp_line = (char *) malloc(sizeof(char)*100);
	while(fgets(temp_line, 56, f_in) != NULL)
	{
		if(temp_line == NULL)
			continue;
		temp_chars = calloc(1, sizeof(struct chars));
		temp_line[strlen(temp_line) - 1] = '\0';
		line_token = strtok(temp_line, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Character Name token.\n");
			return -1;
		}
		temp_name = (char *) calloc(39, sizeof(char));
		strcpy(temp_name, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Team token.\n");
			return -1;
		}
		temp_team = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_team, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Level token.\n");
			return -1;
		}
		temp_level = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_level, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading CID token.\n");
			return -1;
		}
		temp_c_id = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_c_id, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading GuildID token.\n");
			return -1;
		}
		temp_guild_id = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_guild_id, line_token);
		line_token = strtok(NULL, ",");
		if(line_token != NULL)
		{
			fprintf(stderr, "Error reading EOL token.\n");
			return -1;
		}
		t_team = (int) strtol(temp_team, &end_ptr, 10);
		t_level = (int) strtol(temp_level, &end_ptr, 10);
		t_c_id = (int) strtol(temp_c_id, &end_ptr, 10);
		t_guild_id = (int) strtol(temp_guild_id, &end_ptr, 10);
		strcpy(temp_chars->c_name, temp_name);
		temp_chars->team = t_team;
		temp_chars->level = t_level;
		temp_chars->c_id = t_c_id;
		temp_chars->guild_id = t_guild_id;
		current_page++;
		temp_c_page[current_page-1] = temp_chars;
		if(current_page == page_size)
		{
			/* sorting page */
			max = 0;
			total_pages++;
			while(max == 0)
			{
				max = 1;
				for(i = 0; i < page_size-1; i++)
				{
					if(temp_c_page[i]->guild_id > temp_c_page[i+1]->guild_id)
					{
						max = 0;
						inter_char = temp_c_page[i];
						temp_c_page[i] = temp_c_page[i+1];
						temp_c_page[i+1] = inter_char;	
					}
				}
			} 
	
			/* write page */
			for(i = 0; i < page_size; i++)
			{
				/*printf("Page Record %d: %s, %d\n", i+1, temp_c_page[i]->c_name, temp_c_page[i]->guild_id);
				*/fwrite(temp_c_page[i], sizeof(struct chars), 1, f_out);
				free(temp_c_page[i]);
			}
			/* write sentinel value to indicate end of page */
			/*fwrite(sent_char, 1, strlen(sent_char), f_out);*/
			current_page = 0;
		}	
		free(temp_name);
		free(temp_team);
		free(temp_level);
		free(temp_c_id);
		free(temp_guild_id);
		
	}

	if(current_page != 0)
	{
		total_pages++;
		for(i = 0; i < current_page; i++)
		{
			/*printf("Page Record %d: %s, %d\n", i+1, temp_c_page[i]->c_name, temp_c_page[i]->guild_id);
			*/fwrite(temp_c_page[i], sizeof(struct chars), 1, f_out);
			free(temp_c_page[i]);
		}
	}
	free(temp_line);
	rewind(f_out);
	free(temp_c_page);
	fclose(f_in);
	fclose(f_out);
	return total_pages;
}

int read_unsorted_guilds_file(char * filename, int page_size, int buffers_no)
{
	int curr_records = 0, total_pages = 0;
	FILE * f_in;
	FILE * f_out;
	struct guilds * temp_guilds, * inter_guild, ** temp_g_page = calloc(page_size, sizeof(struct guilds));
	char * temp_id;
	char * temp_name;
	char * temp_line;
	char * line_token;
	char * end_ptr;
	char * sent_char = "-1";
	int current_page = 0, i, max;
	int t_id;
	
	f_in = fopen(filename, "r");
	if(f_in == NULL)
	{
		fprintf(stderr, "Guilds file open failure.\n");
		return -1;
	}
	f_out = fopen("temp_0.bin", "w+b");
	if(f_out == NULL)
	{
		fprintf(stderr, "Output file open failure.\n");
		return -1;
	}
	temp_line = (char *) malloc(55);
	while(fgets(temp_line, 56, f_in) != NULL)
	{
		temp_guilds = calloc(1, sizeof(struct chars));
		temp_line[strlen(temp_line) - 1] = '\0';
		line_token = strtok(temp_line, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Guild ID token.\n");
			return -1;
		}
		temp_id = (char *) calloc(1, strlen(line_token));
		strcpy(temp_id, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Name token.\n");
			return -1;
		}
		temp_name = (char *) calloc(49, sizeof(char));
		strcpy(temp_name, line_token);
		line_token = strtok(NULL, ",");
		if(line_token != NULL)
		{
			fprintf(stderr, "EOL token.\n");
			return -1;
		}	
		t_id = (int) strtol(temp_id, &end_ptr, 10);
		strcpy(temp_guilds->g_name, temp_name);
		temp_guilds->guild_id = t_id;	
		
		/* add record to page */
		current_page++;
		temp_g_page[current_page-1] = temp_guilds;
	
		/* full page */
		if(current_page == page_size)
		{
			/* sorting page */
			total_pages++;
			max = 0;
			while(max == 0)
			{
				max = 1;
				for(i = 0; i < page_size - 1; i++)
				{
					if(temp_g_page[i]->guild_id > temp_g_page[i+1]->guild_id)
					{
						max = 0;
						inter_guild = temp_g_page[i];
						temp_g_page[i] = temp_g_page[i+1];
						temp_g_page[i+1] = inter_guild;	
					}
				}
			} 
	
			/* write page */
			for(i = 0; i < page_size; i++)
			{
				printf("Page Record %d: %s, %d\n", i+1, temp_g_page[i]->g_name, temp_g_page[i]->guild_id);
				fwrite(temp_g_page[i], sizeof(struct guilds), 1, f_out);
				free(temp_g_page[i]);
			}
			/* write sentinel value to indicate end of page */
			/*fwrite(sent_char, 1, strlen(sent_char), f_out);*/
			current_page = 0;
		}	
		free(temp_name);
		free(temp_id);
		
	}

	if(current_page != 0)
	{
		total_pages++;
		for(i = 0; i < current_page; i++)
		{
			printf("Page Record %d: %s, %d\n", i+1, temp_g_page[i]->g_name, temp_g_page[i]->guild_id);
			fwrite(temp_g_page[i], sizeof(struct guilds), 1, f_out);
			free(temp_g_page[i]);
		}
	}
	rewind(f_out);
	free(temp_g_page);
	fclose(f_in);
	fclose(f_out);
	return total_pages;
}

int merge_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size)
{
	/* when reading in pages if a all record's in a page are read and not end of run
 *	read next page in run, keep sorting until whole run done */
	
	/* create array for buffer */
	struct chars *** buffer = calloc(buffers_no - 1, sizeof(struct chars));

	/* position in file for next record in run */
	long int * file_pos = calloc(buffers_no - 1, sizeof(long int));

	/* Number of records that have been read from run */
	int * page_count = calloc(buffers_no - 1, sizeof(int));

	struct chars * temp_char = calloc(1, sizeof(struct chars));	
	long int prev = -1;	

	int old_f_no = *file_no - 1;
	char new_output[15];
	char old_output[15];
	char new_number[3];
	char old_number[3];
	char * extension = ".bin";
	FILE * f_in, * f_out;
	int i, j, k, eof_flag = 0, eo_run_group_flag = 0, end_of_array, buffers_used = 0, run_recs = 0;
	int recs_read = 0, recs_write = 0, new_runs, curr;
	
	/*Create new temp file name to write*/
	strcpy(new_output, "temp_");
	sprintf(new_number, "%d", *file_no);
	strcat(new_output, new_number);
	strcat(new_output, extension);
	/*printf("New file name: %s\n", new_output);*/
	
	/*Get previous temp file to open to read*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", old_f_no);
	strcat(old_output, old_number);
	strcat(old_output, extension);
	/*printf("Old file name: %s\n", old_output);*/
	
	/* open new and old file */
	
	f_in = fopen(old_output, "rb");
	if(!f_in)
	{
		fprintf(stderr, "Error opening old temp file.\n");
		return -1;
	}
	/* set run offsets to 0 */	
	for(i = 0; i < buffers_no - 1; i++)
	{
		page_count[i] = 0;
		file_pos[i] = -1;
	}
	printf("OLD FILE: %s\n", old_output);
	rewind(f_in);
	/* read in records */
	i = 0;
	j = 0;
	buffer[i] = calloc(page_size, sizeof(struct chars));
	buffer[i][j] = calloc(1, sizeof(struct chars));
	new_runs = total_runs;
	while(i < buffers_no - 1)
	{
		while(j < page_size)
		{
			if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
			{	
				eof_flag = 1;
				buffer[i][j] = NULL;
				printf("EOF FOUND\n");
				break;
			}
			if(buffer[i][j] == NULL)
				continue;
			recs_read += 1;
			/*printf("%s, %d\n", buffer[i][j]->c_name, buffer[i][j]->guild_id);*/
			j++;
			if( j < page_size)
				buffer[i][j] = calloc(1, sizeof(struct chars));
		}
		
		/* position of next page to read in */
		file_pos[i] = ftell(f_in);
		
		/* Start read records counter */
		page_count[i] += 1;
		/*printf("%d\n", i);*/

	
		if(eof_flag == 1)
		{
			/* sort file */
			if(create_run(new_output, &buffer, page_size, buffers_no, i, &run_recs, &recs_write) == -1)
				return -1;

			if(page_count[i - 1] < (*run_size))
			{
				prev = i;
				for(i = 0; i <= prev; i++)
				{
					for(j = 0; j < page_size; j++)
						free(buffer[i][j]);
					free(buffer[i]);
				}
				printf("RECORDS IN RUN: %d\n", recs_read);
				run_recs = 0;
				i = 0;
				j = 0;
				printf("its habbeding\n");
				/*printf("RUN SIZE: %d, new-2: %d\n", *run_size, page_count[new_runs - 2]);*/
				fseek(f_in, file_pos[i], SEEK_SET);
				buffer[i] = calloc(page_size, sizeof(struct chars));
				buffer[i][j] = calloc(1, sizeof(struct chars));
				eof_flag = 0;
				new_runs = total_runs - 1;
				continue;
			}
			else
			{		
					end_of_array = i;
					buffers_used += i + 1;
					printf("RECORDS READ: %d\n", recs_read);
					break;
			}
		}
				
		i++;		
		j = 0;
		
		/*printf("i: %d, buffers_no: %d, *run_size: %d, last page_count: %d\n", i, buffers_no, *run_size, page_count[buffers_no - 2]);	*/
	
		if((new_runs < (buffers_no - 1)) && i == new_runs)
		{
			if(create_run(new_output, &buffer, page_size, buffers_no, i-1, &run_recs, &recs_write) == -1)
				return -1;
			prev = i;
			if(page_count[new_runs - 1] == *run_size)
			{
				end_of_array = i - 1;
				buffers_used += i + 1;
				printf("TOTAL RUNS: %d, RUN SIZE: %d, PAGE COUNT: %d\n", new_runs, *run_size, page_count[new_runs - 1]);
				break;
			}
			for(i = 0; i < prev;  i++)
			{
				for(j = 0; j < page_size; j++)
					free(buffer[i][j]);
				free(buffer[i]);	
			}
			printf("RECORDS IN RUN: %d\n", run_recs);
			run_recs = 0;
			i = 0;
			j = 0;
		}	
		/* If buffers are full or all runs are at the end */
		else if(i == (buffers_no - 1) || page_count[buffers_no - 2] == (*run_size) || i == prev)
		{	
			/* Call merge function reset i to 0 */
			if(create_run(new_output, &buffer, page_size, buffers_no, i-1, &run_recs, &recs_write) == -1)
				return -1;

			curr = i;
			/* free memeory of buffer array */
			for(i = 0; i < curr; i++)
			{
				for(j = 0; j < page_size; j++)
					free(buffer[i][j]);
				free(buffer[i]);	
			}
			/* If at run end, go to start of next group of runs */
			if(page_count[buffers_no - 2] == (*run_size))
			{
				for(i = 0; i < buffers_no -1; i++)
				{
					file_pos[i] = -1;
					page_count[i] = 0;
				}
				i = 0; 
				j = 0;
				eo_run_group_flag = 1;
				buffers_used += buffers_no - 1;
				printf("RECORDS IN RUN: %d\n", run_recs);
				run_recs = 0;
				/* Should be at start of next run already */
				buffer[i] = calloc(page_size, sizeof(struct chars));
				buffer[i][j] = calloc(1, sizeof(struct chars));
				continue;
			}
			i = 0;
			j = 0;
		}	
		buffer[i] = calloc(page_size, sizeof(struct chars));
		buffer[i][j] = calloc(1, sizeof(struct chars));

		/* Move to start of next page of next run */
		if(file_pos[i] == -1 && eo_run_group_flag == 0 && i > 0)
		{
			/*printf("VALUES: RUN SIZE: %d, PAGE SIZE: %d, PAGE COUNT PREV: %d\n", *run_size, page_size, page_count[i-1]);*/
			/*printf("OPTION A - %d\n", (((*run_size) * page_size) - (page_count[i-1] * page_size)));*/
			fseek(f_in, (((*run_size) * page_size)-(page_count[i-1] * page_size))*sizeof(struct chars), SEEK_CUR);
		}
		else if(eo_run_group_flag == 1)
		{
			/*printf("OPTION B\n");*/
			eo_run_group_flag = 0;
		}
		else 
		{
			/*printf("OPTION C - %d\n", i);*/
			fseek(f_in, file_pos[i], SEEK_SET);
		}
	}
	
	*file_no += 1;
	for(k = 0; k <= end_of_array; k++)
	{
		for(j = 0; j < page_size; j++)
		{
			free(buffer[k][j]);
		}
		free(buffer[k]);
	}

	/* CHECK IF FILE IS AT END OF FILE
 * 	IF NOT, APPEND REST OF FILE 
 * 	ALSO, OUTPUT THE CURRENT SIZE OF EACH RUN, TO CHECK IF THEY'RE DECREASING
 *
 * 	CHECK IN INPUT FUNCTION FOR IF RECORD IS NULL, SKIP IT.*/
	prev = ftell(f_in);
	/*if(!feof(f_in))
	{
		fseek(f_in, prev, SEEK_SET);
		f_out = fopen(new_output, "ab");
		if(!f_out)
		{
			fprintf(stderr, "Error opening output file for appendage.\n");
			return -1;
		}
		printf("**reading extra data**\n");
		while(fread(temp_char, sizeof(struct chars), 1, f_in) == 1)
		{
			fwrite(temp_char, sizeof(struct chars), 1, f_out);
		}
		fclose(f_out);
	}*/
	i = 0;
	f_out = fopen(new_output, "rb");
	rewind(f_out);
	while(fread(temp_char, sizeof(struct chars), 1, f_out) == 1)
		i++;
	printf("NUMBER OF RECORDS: %d\n", i);
	fclose(f_out);
	free(temp_char);
	free(file_pos);
	free(page_count);
	fclose(f_in);
	/* Remove old temp file */
	/*remove(new_output);*/
	remove(old_output);
	printf("******* RECORDS WRITTEN: %d\n", recs_write);
	printf("Previous run size: %d\n", *run_size);
	printf("Number of buffers used: %d\n", buffers_used);	
	printf("Previous total runs: %d\n", total_runs);
	if(buffers_used > (buffers_no - 1))
		*run_size = (*run_size) * (buffers_no - 1);
	else
		*run_size = (*run_size) * buffers_used;
	if((*run_size) > (10001/page_size))
		*run_size = (int)ceil((10000.0/((double)page_size)));
	printf("New run size: %d\n", *run_size);
	printf("TEST: %.2f\n", ((double)total_runs)/((double)(buffers_no-1)));
	total_runs = (int)ceil(((double)total_runs)/((double)(buffers_no-1)));
	/*total_runs = (int)ceil(((double)total_runs)/((double)buffers_used));*/
	if(total_runs <= 0)
		total_runs = 1;
	printf("New total runs: %d\n", total_runs);	
	printf("NEW FILE: %s\n", new_output);	
	return total_runs;
}

int create_run(char * output_file, struct chars **** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write)
{
	FILE * f_out;
	int i, j, curr_min = 0, curr_rec = 0, records_flag = 1, runs_completed = 0;
	f_out = fopen(output_file, "ab");
	if(!f_out)
	{
		fprintf(stderr, "Error opening new temp file.\n");
		return -1;
	}	
	
	j = 0;
	i = 0;
	/*printf("%s, %d\n", (*buffer)[0][0]->c_name, (*buffer)[0][0]->guild_id);*/
	/*printf("------------\n");*/
	while(records_flag == 1)
	{
		records_flag = 0;
		curr_rec = 0;
		curr_min = 0;
		for(i = 0; (i < buffers_no - 1 ); i++)
		{
			j = 0;
			if( i > end)
				break;
			if((*buffer)[i] == NULL)
				continue;
			while((*buffer)[i][j] == NULL)
			{
				j++;
				if(j == page_size)
					break;
			}
			/*printf("%s, %d\n", (*buffer)[0][0]->c_name, (*buffer)[0][0]->guild_id);*/
		
			if(j == page_size)
				continue;
			/*if((*buffer)[i][j] == NULL)
				continue;*/			
	
			records_flag = 1;

			/* check for default min record isn't NULL */
			if((*buffer)[curr_min][curr_rec] == NULL)
			{
				curr_min = i;
				curr_rec = j;
			}

			/* check for min record */
			else if((*buffer)[i][j]->guild_id < (*buffer)[curr_min][curr_rec]->guild_id)
			{
				curr_min = i;
				curr_rec = j;
			}
		}
		if(records_flag != 0)
		{
			if(fwrite((*buffer)[curr_min][curr_rec], sizeof(struct chars), 1, f_out) != 1)
			{
				printf("Error writing line!\n");
				return -1;
			}
			(*buffer)[curr_min][curr_rec] = NULL;
			/*if(curr_rec == page_size - 1)
				(*buffer)[curr_min] = NULL;*/
			*run_recs += 1;
			*recs_write += 1;
		}
	}
	/*printf("------------\n");*/
	fclose(f_out);
	return 1;
}
