#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#pragma pack(1)
typedef struct chars
{
	char c_name[39];
	int team;
	int level;
	int c_id;
	int guild_id;
} chars;

typedef struct guilds
{
	int guild_id;
	char g_name[49];
} guilds;

/* make seperate function for merge sort */
/* Make array of pages, which is the buffer.
 * Each page is an array of the specified number of structs
 * Merge sort uses buffers_no - 1 buffers as input to merge, 
 * each buffer stores 1 page */

/* Functions for reading the unsorted characters and guilds files.
 * It reads file line-by-line, tokenising them as apporpriate for the 
 * file type and writing to a temporary struct. The record is then inserted 
 * into the temporary page. Once the page is full, sort the page using a selection
 * sort then write the ordered page to a binary file. Once the end of file is reached, sort and write
 * and remaining records then return the total number of runs.*/
int read_unsorted_character_file(char * filename, int page_size, int buffers_no);
int read_unsorted_guilds_file(char * filename, int page_size, int buffers_no);

/* Loading pages from the runs into the buffer, and calling on the run creation function.
 * Function first readings in bufers_no - 1 pages into the buffer, or until the EOF (whatever comes first). 
 * After reading each page into memory, if either; 
 * 	End of Buffer is reached
 * 	Final run is reached
 * 	End of file is reached
 * Begin the process of merging all current runs in the buffer into a single new run. Each time a page
 * is emptied in the merging process and that run has not reached the end, load the next page in place
 * of the now empty buffer place. Keep doing this until all runs in the buffer are merged. Repeat this
 * process until the end of the file is reached. The function creates one new file per call (pass), which
 * all new runs are written to*/
int merge_chars_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size);

/* Merging pages into a single run in a new binary file. 
 * Function iterates through whole buffer, until it finds the smallest current guild_id. This record is then
 * written to the new binary file. Repeat for next smallest record. If while iterating through the buffer a
 * page is found to have all records written out (and the run that page belongs to has not been entirely read),
 * then stop reading the buffer and return that buffer place number to load in a new page. Repeat until the
 * buffers are all empty and all pages of all runs have been read, then return -2 (End of merge).*/
int create_chars_run(char * output_file, struct chars *** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write, int * page_count, int run_size);

/* Same operation as merge_chars_pages and create_chars_run, but for guilds files */
int merge_guilds_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size);
int create_guilds_run(char * output_file, struct guilds *** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write, int * page_count, int run_size);

int main(int argc, char * argv[])
{
	int page_size, buffers_no, total_runs, new_pages, new_runs;
	int file_counter = 1, run_counter = 1;
	int * file_no, * run_size;
	char * end_ptr;
	
	char old_output[15];
	char sorted_output[50];
	char old_number[3];
	char * sort_exten = ".sorted";
	char * extension = ".bin";
	FILE * final_in, * final_out;
	struct chars * temp_char = calloc(1, sizeof(struct chars));
	struct guilds * temp_guild = calloc(1, sizeof(struct guilds));
	
	/* Argument checking and conversion */	
	if(argc != 5)
	{
		fprintf(stderr, "Insufficient number of arguments given.\n");
		return 1;
	}
	page_size = (int) strtol(argv[1], &end_ptr, 10);
	buffers_no = (int) strtol(argv[2], &end_ptr, 10);
	run_size = &run_counter;

	/* Reading unsorted file into initial sorted pages */
	if(strcmp(argv[4], "c") == 0)
	{
		total_runs = read_unsorted_character_file(argv[3], page_size, buffers_no);
		if(total_runs == -1)
			return 1;
	}
	else if(strcmp(argv[4], "g") == 0)
	{
		total_runs = read_unsorted_guilds_file(argv[3], page_size, buffers_no);
		if(total_runs == -1)
			return 1;
	}
	else
	{
		fprintf(stderr, "Please use either the c or g flag.\n");
		return 1;
	}
	file_no = &file_counter;

	/* Merging runs into sorted file */
	while(total_runs > 1)
 	{
		printf("TEST HERE: %d\n", *file_no);
		if(strcmp(argv[4], "c") == 0)
			new_runs = merge_chars_pages(file_no, page_size, buffers_no, total_runs, run_size);
		else if(strcmp(argv[4], "g") == 0)
			new_runs = merge_guilds_pages(file_no, page_size, buffers_no, total_runs, run_size);
		if(new_runs == -1)
		{
			fprintf(stderr, "program exiting...\n");
			return 1;
		}
  		total_runs = new_runs;
		printf("--------------\n");
  	}
	
	printf("total_runs: %d\n", total_runs); 
			
	/*Get name of final sorted temp file*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", (*file_no) - 1);
	strcat(old_output, old_number);
	strcat(old_output, extension);
	
	/* Create name of final sorted output file */
	strcpy(sorted_output, argv[3]);
	strcat(sorted_output, sort_exten);


	/* Open final binary file and output file for processing */
	final_in = fopen(old_output, "r+b");
	if(!final_in)
	{
		fprintf(stderr, "Error opening final temp file.\n");
		return 1;
	}


	final_out = fopen(sorted_output, "w+");
	if(!final_out)
	{
		fprintf(stderr, "Error opening sorted file.\n");
		return 1;
	}
	/* write sorted binary file to sorted file */
	if(strcmp(argv[4], "c") == 0)
	{
		while(fread(temp_char, sizeof(struct chars), 1, final_in) == 1)
		{
			fprintf(final_out, "%s,%d,%d,%d,%d\n", temp_char->c_name, temp_char->team, temp_char->level,
				temp_char->c_id, temp_char->guild_id);
		}
	}
	else if(strcmp(argv[4], "g") == 0)
	{
		while(fread(temp_guild, sizeof(struct guilds), 1, final_in) == 1)
		{
			fprintf(final_out, "%d,%s\n", temp_guild->guild_id, temp_guild->g_name);
		}
	}	
	/*remove temporary binary file */
	fclose(final_in);
	fclose(final_out);
	remove(old_output);
	free(temp_char);
	free(temp_guild);
	return 0;
}

int read_unsorted_character_file(char * filename, int page_size, int buffers_no)
{
	int curr_records = 0, total_pages = 0;
	FILE * f_in;
	FILE * f_out;
	struct chars * temp_chars, * inter_char, ** temp_c_page = calloc(page_size, sizeof(struct chars));
	char * temp_line;
	char * line_token;
	char * temp_name;
	char * temp_team;
	char * temp_level;
	char * temp_c_id;
	char * temp_guild_id;
	char * end_ptr;
	int current_page = 0, i, max;
	unsigned int t_team, t_level, t_c_id, t_guild_id;
	
	/* Opening original file for reading and first binary file */		
	f_in = fopen(filename, "r");
	if(f_in == NULL)
	{
		fprintf(stderr, "Characters file open failure.\n");
		return -1;
	}
	f_out = fopen("temp_0.bin", "w+b");
	if(f_out == NULL)
	{
		fprintf(stderr, "Output file open failure.\n");
		return -1;
	}
	temp_line = (char *) malloc(sizeof(char)*100);

	/* Reading file lines and tokenising */
	while(fgets(temp_line, 56, f_in) != NULL)
	{
		if(temp_line == NULL)
			continue;
		temp_chars = calloc(1, sizeof(struct chars));
		temp_line[strlen(temp_line) - 1] = '\0';
		line_token = strtok(temp_line, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Character Name token.\n");
			return -1;
		}
		temp_name = (char *) calloc(39, sizeof(char));
		strcpy(temp_name, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Team token.\n");
			return -1;
		}
		temp_team = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_team, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Level token.\n");
			return -1;
		}
		temp_level = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_level, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading CID token.\n");
			return -1;
		}
		temp_c_id = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_c_id, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading GuildID token.\n");
			return -1;
		}
		temp_guild_id = (char *) calloc(1, strlen(line_token)+1);
		strcpy(temp_guild_id, line_token);
		line_token = strtok(NULL, ",");
		if(line_token != NULL)
		{
			fprintf(stderr, "Error reading EOL token.\n");
			return -1;
		}

		/* Writing tokens to temporary struct */
		t_team = (int) strtol(temp_team, &end_ptr, 10);
		t_level = (int) strtol(temp_level, &end_ptr, 10);
		t_c_id = (int) strtol(temp_c_id, &end_ptr, 10);
		t_guild_id = (int) strtol(temp_guild_id, &end_ptr, 10);
		strcpy(temp_chars->c_name, temp_name);
		temp_chars->team = t_team;
		temp_chars->level = t_level;
		temp_chars->c_id = t_c_id;
		temp_chars->guild_id = t_guild_id;

		/* Insert record into temp page */
		current_page++;
		temp_c_page[current_page-1] = temp_chars;
		if(current_page == page_size)
		{
			/* sorting page */
			max = 0;
			total_pages++;
			while(max == 0)
			{
				max = 1;
				for(i = 0; i < page_size-1; i++)
				{
					if(temp_c_page[i]->guild_id > temp_c_page[i+1]->guild_id)
					{
						max = 0;
						inter_char = temp_c_page[i];
						temp_c_page[i] = temp_c_page[i+1];
						temp_c_page[i+1] = inter_char;	
					}
				}
			} 
	
			/* write page */
			for(i = 0; i < page_size; i++)
			{
				fwrite(temp_c_page[i], sizeof(struct chars), 1, f_out);
				free(temp_c_page[i]);
			}
			current_page = 0;
		}	
		free(temp_name);
		free(temp_team);
		free(temp_level);
		free(temp_c_id);
		free(temp_guild_id);
		
	}

	/*Write any remaining records in temp page to file */
	if(current_page != 0)
	{
		total_pages++;
		for(i = 0; i < current_page; i++)
		{
			fwrite(temp_c_page[i], sizeof(struct chars), 1, f_out);
			free(temp_c_page[i]);
		}
	}
	free(temp_line);
	rewind(f_out);
	free(temp_c_page);
	fclose(f_in);
	fclose(f_out);
	return total_pages;
}

int read_unsorted_guilds_file(char * filename, int page_size, int buffers_no)
{
	int curr_records = 0, total_pages = 0;
	FILE * f_in;
	FILE * f_out;
	struct guilds * temp_guilds, * inter_guild, ** temp_g_page = calloc(page_size, sizeof(struct guilds));
	char * temp_id;
	char * temp_name;
	char * temp_line;
	char * line_token;
	char * end_ptr;
	char * sent_char = "-1";
	int current_page = 0, i, max;
	int t_id;
	
	f_in = fopen(filename, "r");
	if(f_in == NULL)
	{
		fprintf(stderr, "Guilds file open failure.\n");
		return -1;
	}
	f_out = fopen("temp_0.bin", "w+b");
	if(f_out == NULL)
	{
		fprintf(stderr, "Output file open failure.\n");
		return -1;
	}
	temp_line = (char *) malloc(55);
	while(fgets(temp_line, 56, f_in) != NULL)
	{
		temp_guilds = calloc(1, sizeof(struct chars));
		temp_line[strlen(temp_line) - 1] = '\0';
		line_token = strtok(temp_line, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Guild ID token.\n");
			return -1;
		}
		temp_id = (char *) calloc(1, strlen(line_token));
		strcpy(temp_id, line_token);
		line_token = strtok(NULL, ",");
		if(line_token == NULL)
		{
			fprintf(stderr, "Error reading Name token.\n");
			return -1;
		}
		temp_name = (char *) calloc(49, sizeof(char));
		strcpy(temp_name, line_token);
		line_token = strtok(NULL, ",");
		if(line_token != NULL)
		{
			fprintf(stderr, "EOL token.\n");
			return -1;
		}	
		t_id = (int) strtol(temp_id, &end_ptr, 10);
		strcpy(temp_guilds->g_name, temp_name);
		temp_guilds->guild_id = t_id;	
		
		/* add record to page */
		current_page++;
		temp_g_page[current_page-1] = temp_guilds;
	
		/* full page */
		if(current_page == page_size)
		{
			/* sorting page */
			total_pages++;
			max = 0;
			while(max == 0)
			{
				max = 1;
				for(i = 0; i < page_size - 1; i++)
				{
					if(temp_g_page[i]->guild_id > temp_g_page[i+1]->guild_id)
					{
						max = 0;
						inter_guild = temp_g_page[i];
						temp_g_page[i] = temp_g_page[i+1];
						temp_g_page[i+1] = inter_guild;	
					}
				}
			} 
	
			/* write page */
			for(i = 0; i < page_size; i++)
			{
				/*printf("Page Record %d: %s, %d\n", i+1, temp_g_page[i]->g_name, temp_g_page[i]->guild_id);*/
				fwrite(temp_g_page[i], sizeof(struct guilds), 1, f_out);
				free(temp_g_page[i]);
			}
			/* write sentinel value to indicate end of page */
			/*fwrite(sent_char, 1, strlen(sent_char), f_out);*/
			current_page = 0;
		}	
		free(temp_name);
		free(temp_id);
		
	}

	if(current_page != 0)
	{
		total_pages++;
		for(i = 0; i < current_page; i++)
		{
			/*printf("Page Record %d: %s, %d\n", i+1, temp_g_page[i]->g_name, temp_g_page[i]->guild_id);*/
			fwrite(temp_g_page[i], sizeof(struct guilds), 1, f_out);
			free(temp_g_page[i]);
		}
	}
	rewind(f_out);
	free(temp_g_page);
	fclose(f_in);
	fclose(f_out);
	return total_pages;
}

int merge_chars_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size)
{
	/* when reading in pages if a all record's in a page are read and not end of run
 *	read next page in run, keep sorting until whole run done */
	
	/* create array for buffer */
	struct chars *** buffer = calloc(buffers_no - 1, sizeof(struct chars));

	/* position in file for next record in run */
	long int * file_pos = calloc(buffers_no - 1, sizeof(long int));

	/* Number of records that have been read from run */
	int * page_count = calloc(buffers_no - 1, sizeof(int));

	struct chars * temp_char = calloc(1, sizeof(struct chars));	
	long int prev = -1;	

	int old_f_no = *file_no - 1;
	char new_output[15];
	char old_output[15];
	char new_number[3];
	char old_number[3];
	char * extension = ".bin";
	FILE * f_in, * f_out;
	int i, j, k, eof_flag = 0, eo_run_group_flag = 0, end_of_array, buffers_used = 0, run_recs = 0;
	int recs_read = 0, recs_write = 0, new_runs, read_done, write_return, write_complete = 0, move_places;
	
	/*Create new temp file name to write*/
	strcpy(new_output, "temp_");
	sprintf(new_number, "%d", *file_no);
	strcat(new_output, new_number);
	strcat(new_output, extension);
	/*printf("New file name: %s\n", new_output);*/
	
	/*Get previous temp file to open to read*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", old_f_no);
	strcat(old_output, old_number);
	strcat(old_output, extension);
	/*printf("Old file name: %s\n", old_output);*/
	
	/* open new and old file */
	
	f_in = fopen(old_output, "rb");
	if(!f_in)
	{
		fprintf(stderr, "Error opening old temp file.\n");
		return -1;
	}
	/* set run offsets to 0 */	
	for(i = 0; i < buffers_no - 1; i++)
	{
		page_count[i] = 0;
		file_pos[i] = -1;
	}
	printf("OLD FILE: %s\n", old_output);
	rewind(f_in);
	/* read in records */
	i = 0;
	j = 0;
	buffer[i] = calloc(page_size, sizeof(struct chars));	
	buffer[i][j] = calloc(1, sizeof(struct chars));
	new_runs = total_runs;
	while(1)
	{
		/*printf("%d\n", i);*/
		read_done = 0;
		while(j < page_size)
		{
			if(eof_flag == 1)
				break;
			if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
			{	
				eof_flag = 1;
				buffer[i][j] = NULL;
				printf("EOF FOUND\n");
				page_count[i] = (*run_size);
				break;
			}
			if(buffer[i][j] == NULL)
				continue;
			read_done = 1;
			recs_read += 1;
			/*printf("%s, %d\n", buffer[i][j]->c_name, buffer[i][j]->guild_id);*/
			j++;
			if( j < page_size)
			{
				buffer[i][j] = calloc(1, sizeof(struct chars));	
			}
		}
		
		/* position of next page to read in */
		file_pos[i] = ftell(f_in);
		
		/* Start read records counter */
		/*if(read_done == 1)*/
			page_count[i] += 1;
		/*printf("%d\n", i);*/

	
		if(eof_flag == 1)
		{
			write_complete = 0;
			prev = i;
			printf("EOF ENTERED.\n");
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_chars_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						/*printf("RECORDS READ: %d\n", run_recs);*/
						run_recs = 0;
						write_complete = 1;
				}
				else
				{
					fseek(f_in, file_pos[write_return], SEEK_SET);
					j = 0;
					i = write_return;
					/*printf("write return: %d\n", write_return);*/
					read_done = 0;
					while(j < page_size)
					{
						if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
						{
							page_count[i] = (*run_size);
							buffer[i][j] = NULL;
							eof_flag = 1;
							break;
						}
						if(eof_flag == 1)
							break;
						if(buffer[i][j] == NULL)
							continue;
						read_done = 1;
						recs_read += 1;
						j++;
					}
					/*if(read_done == 1)*/
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
			}
			write_complete = 0;
			break;
		}
		else if(i == (total_runs - 1))
		{
			write_complete = 0;
			prev = i;
			printf("here\n");
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_chars_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						/*printf("RECORDS READ: %d\n", run_recs);*/
						write_complete = 1;
						recs_read = 0;
						run_recs = 0;
				}
				else
				{
					j = 0;
					/*printf("Write Return: %d\n", write_return);
					printf("Tot Runs - 1: %d\n", total_runs - 1);*/
					i = write_return;
					fseek(f_in, file_pos[i], SEEK_SET);
					read_done = 0;
					while(j < page_size)
					{
						/*printf("test\n");*/
						if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
						{
							page_count[i] = (*run_size);
							buffer[i][j] = NULL;
							eof_flag = 1;
							break;
						}
						if(buffer[i][j] == NULL)
							continue;
						recs_read += 1;
						read_done = 1;
						j++;
					}
					if(eof_flag == 1)
						break;
					/*if(read_done == 1)*/
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
				if(eof_flag == 1)
					break;
			}
			if(eof_flag == 1)
				continue;
			end_of_array = prev;
			buffers_used += i + 1;
			write_complete = 0;
			eof_flag = 1;
			/*break;*/	
				
		}		
		/* If buffers are full or all runs are at the end */
		else if(i == (buffers_no - 2))
		{		
			write_complete = 0;
			prev = i;
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_chars_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						/*printf("RECORDS READ: %d\n", run_recs);*/
						write_complete = 1;
						recs_read = 0;
						run_recs = 0;
						fseek(f_in, file_pos[prev], SEEK_SET);
						for(i = 0; i < buffers_no-1; i++)
						{
							file_pos[i] = -1;
							page_count[i] = 0;
							for(j = 0; j < page_size; j++)
								free(buffer[i][j]);
							free(buffer[i]);
						}
				}
				else
				{
					fseek(f_in, file_pos[write_return], SEEK_SET);
					j = 0;
					i = write_return;
					read_done = 0;
					while(j < page_size)
					{
						if(fread(buffer[i][j], sizeof(struct chars), 1, f_in) != 1)
						{
							page_count[i] = (*run_size);
							buffer[i][j] = NULL;
							eof_flag = 1;
							break;
						}
						if(buffer[i][j] == NULL)
							continue;
						recs_read += 1;
						read_done = 1;
						j++;
					}
					if(eof_flag == 1)
						break;
					/*if(read_done == 1)*/
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
			}
			if(eof_flag == 1)
				continue;
			end_of_array = prev;
			buffers_used += prev + 1;
			write_complete = 0;
			i = 0;
			j = 0;
			buffer[i] = calloc(page_size, sizeof(struct chars));
			buffer[i][j] = calloc(1, sizeof(struct chars));
			continue;
		}
		move_places = ((*run_size)*page_size)-(page_count[i]*page_size);
		/*printf("move_places: %d\n", move_places);*/
		if(move_places > 0)
			fseek(f_in, move_places*sizeof(struct chars), SEEK_CUR);		
		i++;
		j = 0;
		/*printf("new calloc at pos %d\n", i);
		if(i==50)
		{
			printf("at 50");
		}*/
		buffer[i] = calloc(page_size, sizeof(struct chars));	
		if(buffer[i]==NULL){printf("calloc ERROR\n");}
		buffer[i][j] = calloc(1, sizeof(struct chars));	
		if(buffer[i][j]==NULL){printf("calloc ERROR\n");}
	
	}
	
	*file_no += 1;
	for(k = 0; k <= end_of_array; k++)
	{
		if(page_count[k] < *run_size)
			printf("RUN %d NOT FULLY READ.\n", k);
		for(j = 0; j < page_size; j++)
		{
			free(buffer[k][j]);
		}
		free(buffer[k]);
	}

	/* CHECK IF FILE IS AT END OF FILE
 * 	IF NOT, APPEND REST OF FILE 
 * 	ALSO, OUTPUT THE CURRENT SIZE OF EACH RUN, TO CHECK IF THEY'RE DECREASING
 *
 * 	CHECK IN INPUT FUNCTION FOR IF RECORD IS NULL, SKIP IT.*/
	/*if(((buffers_no - 1) > total_runs) && eof_flag != 1)
	{
		fseek(f_in, file_pos[buffers_no - 2], SEEK_SET);
	}
	else if(((buffers_no - 1) < total_runs) && eof_flag != 1)
		fseek(f_in, file_pos[total_runs], SEEK_SET); 
	prev = ftell(f_in);
	if(!feof(f_in))
	{
		fseek(f_in, prev, SEEK_SET);
		f_out = fopen(new_output, "ab");
		if(!f_out)
		{
			fprintf(stderr, "Error opening output file for appendage.\n");
			return -1;
		}
		printf("**reading extra data**\n");
		while(fread(temp_char, sizeof(struct chars), 1, f_in) == 1)
		{
			fwrite(temp_char, sizeof(struct chars), 1, f_out);
		}
		fclose(f_out);
	}*/
	i = 0;
	f_out = fopen(new_output, "rb");
	rewind(f_out);
	/*while(fread(temp_char, sizeof(struct chars), 1, f_out) == 1)
		i++;
	printf("NUMBER OF RECORDS: %d\n", i);*/
	fclose(f_out);
	free(temp_char);
	free(file_pos);
	free(page_count);
	fclose(f_in);
	/* Remove old temp file */
	/*remove(new_output);*/
	remove(old_output);
	printf("******* RECORDS WRITTEN: %d\n", recs_write);
	printf("Previous run size: %d\n", *run_size);
	printf("Number of buffers used: %d\n", buffers_used);	
	printf("Previous total runs: %d\n", total_runs);
	if(buffers_used > (buffers_no - 1))
		*run_size = (*run_size) * (buffers_no - 1);
	else
		*run_size = (*run_size) * buffers_used;
	if((*run_size) > (10000/page_size))
		*run_size = (int)ceil((10000.0/((double)page_size)));
	printf("New run size: %d\n", *run_size);
	printf("TEST: %.2f\n", ((double)total_runs)/((double)(buffers_no-1)));
	total_runs = (int)ceil(((double)total_runs)/((double)(buffers_no-1)));
	/*total_runs = (int)ceil(((double)total_runs)/((double)buffers_used));*/
	if(total_runs <= 0)
		total_runs = 1;
	printf("New total runs: %d\n", total_runs);	
	printf("NEW FILE: %s\n", new_output);	
	return total_runs;
}

int create_chars_run(char * output_file, struct chars *** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write, int * page_count, int run_size)
{
	FILE * f_out;
	int i, j, curr_min = 0, curr_rec = 0, records_flag = 1, early_end_flag = 0;
	f_out = fopen(output_file, "ab");
	if(!f_out)
	{
		fprintf(stderr, "Error opening new temp file.\n");
		return -1;
	}	
	
	j = 0;
	i = 0;
	/*printf("%s, %d\n", (*buffer)[0][0]->c_name, (*buffer)[0][0]->guild_id);*/
	/*printf("------------\n");*/
	while(records_flag == 1)
	{
		records_flag = 0;
		curr_rec = 0;
		curr_min = 0;
		for(i = 0; (i < buffers_no - 1 ); i++)
		{
			j = 0;
			if( i > end)
				break;
			/*if(buffer[i] == NULL)
			{
				if(page_count[i] >= run_size)
					continue;
				else
				{
					fclose(f_out);
					return i;
				}
			}*/
			/*if(buffer[50][0]==NULL)
			{
				printf("ERROR\n");
			}*/

			if(buffer[i][j] == NULL)
				continue;			

			while(strcmp(buffer[i][j]->c_name, "") == 0)
			{
				j++;
				while(buffer[i][j] == NULL && j < page_size)
				{
					j++;
				}
				if(j == page_size)
					break;
			}
			/*printf("%s, %d\n", (*buffer)[0][0]->c_name, (*buffer)[0][0]->guild_id);*/
		
			/*if(early_end_flag == 1)
			{
				early_end_flag = 0;
				continue;
			}*/
			if(j == page_size)
			{
				if(page_count[i] >= run_size)
					continue;
				else
				{
					fclose(f_out);
					return i;
				}
			}
	
			records_flag = 1;

			/* check for default min record isn't NULL */
			if(strcmp(buffer[curr_min][curr_rec]->c_name, "") == 0)
			{
				curr_min = i;
				curr_rec = j;
			}

			/* check for min record */
			else if(buffer[i][j]->guild_id < buffer[curr_min][curr_rec]->guild_id)
			{
				curr_min = i;
				curr_rec = j;
			}
		}
		if(records_flag != 0)
		{
			/*printf("WRITING: %s, %d\n", buffer[curr_min][curr_rec]->c_name, buffer[curr_min][curr_rec]->guild_id);*/
			if(fwrite(buffer[curr_min][curr_rec], sizeof(struct chars), 1, f_out) != 1)
			{
				printf("Error writing line!\n");
				fclose(f_out);
				return -1;
			}
			/*if(curr_min==50)
			{
				printf("50");
			}*/
			memset(buffer[curr_min][curr_rec], 0, sizeof(struct chars));
			/*if(curr_rec == page_size - 1)
				(*buffer)[curr_min] = NULL;*/
			*run_recs += 1;
			*recs_write += 1;
		}
	}
	/*printf("------------\n");*/
	fclose(f_out);
	return -2;
}


int merge_guilds_pages(int * file_no, int page_size, int buffers_no, int total_runs, int * run_size)
{
	/* when reading in pages if a all record's in a page are read and not end of run
 *	read next page in run, keep sorting until whole run done */
	
	/* create array for buffer */
	struct guilds *** buffer = calloc(buffers_no - 1, sizeof(struct guilds));

	/* position in file for next record in run */
	long int * file_pos = calloc(buffers_no - 1, sizeof(long int));

	/* Number of records that have been read from run */
	int * page_count = calloc(buffers_no - 1, sizeof(int));

	struct guilds * temp_guild = calloc(1, sizeof(struct guilds));	
	long int prev = -1;	

	int old_f_no = *file_no - 1;
	char new_output[15];
	char old_output[15];
	char new_number[3];
	char old_number[3];
	char * extension = ".bin";
	FILE * f_in, * f_out;
	int i, j, k, eof_flag = 0, eo_run_group_flag = 0, end_of_array, buffers_used = 0, run_recs = 0;
	int recs_read = 0, recs_write = 0, new_runs, read_done, write_return, write_complete = 0, move_places;
	
	/*Create new temp file name to write*/
	strcpy(new_output, "temp_");
	sprintf(new_number, "%d", *file_no);
	strcat(new_output, new_number);
	strcat(new_output, extension);
	/*printf("New file name: %s\n", new_output);*/
	
	/*Get previous temp file to open to read*/
	strcpy(old_output, "temp_");
	sprintf(old_number, "%d", old_f_no);
	strcat(old_output, old_number);
	strcat(old_output, extension);
	/*printf("Old file name: %s\n", old_output);*/
	
	/* open new and old file */
	
	f_in = fopen(old_output, "rb");
	if(!f_in)
	{
		fprintf(stderr, "Error opening old temp file.\n");
		return -1;
	}
	/* set run offsets to 0 */	
	for(i = 0; i < buffers_no - 1; i++)
	{
		page_count[i] = 0;
		file_pos[i] = -1;
	}
	printf("OLD FILE: %s\n", old_output);
	rewind(f_in);
	/* read in records */
	i = 0;
	j = 0;
	buffer[i] = calloc(page_size, sizeof(struct guilds));	
	buffer[i][j] = calloc(1, sizeof(struct guilds));
	new_runs = total_runs;
	while(1)
	{
		/*printf("%d\n", i);*/
		read_done = 0;
		while(j < page_size)
		{
			if(eof_flag == 1)
				break;
			if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
			{	
				eof_flag = 1;
				buffer[i][j] = NULL;
				printf("EOF FOUND\n");
				page_count[i] = (*run_size);
				break;
			}
			if(buffer[i][j] == NULL)
				continue;
			read_done = 1;
			recs_read += 1;
			/*printf("%s, %d\n", buffer[i][j]->c_name, buffer[i][j]->guild_id);*/
			j++;
			if( j < page_size)
			{
				buffer[i][j] = calloc(1, sizeof(struct guilds));	
			}
		}
		
		/* position of next page to read in */
		file_pos[i] = ftell(f_in);
		
		/* Start read records counter */
		/*if(read_done == 1)*/
			page_count[i] += 1;
		/*printf("%d\n", i);*/

	
		if(eof_flag == 1)
		{
			write_complete = 0;
			prev = i;
			printf("EOF ENTERED.\n");
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_guilds_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						/*printf("RECORDS READ: %d\n", run_recs);*/
						run_recs = 0;
						write_complete = 1;
				}
				else
				{
					fseek(f_in, file_pos[write_return], SEEK_SET);
					j = 0;
					i = write_return;
					/*printf("write return: %d\n", write_return);*/
					read_done = 0;
					while(j < page_size)
					{
						if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
						{
							page_count[i] = (*run_size);
							buffer[i][j] = NULL;
							eof_flag = 1;
							break;
						}
						if(eof_flag == 1)
							break;
						if(buffer[i][j] == NULL)
							continue;
						read_done = 1;
						recs_read += 1;
						j++;
					}
					/*if(read_done == 1)*/
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
			}
			write_complete = 0;
			break;
		}
		else if(i == (total_runs - 1))
		{
			write_complete = 0;
			prev = i;
			printf("here\n");
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_guilds_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						/*printf("RECORDS READ: %d\n", run_recs);*/
						write_complete = 1;
						recs_read = 0;
						run_recs = 0;
				}
				else
				{
					j = 0;
					/*printf("Write Return: %d\n", write_return);
					printf("Tot Runs - 1: %d\n", total_runs - 1);*/
					i = write_return;
					fseek(f_in, file_pos[i], SEEK_SET);
					read_done = 0;
					while(j < page_size)
					{
						/*printf("test\n");*/
						if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
						{
							page_count[i] = (*run_size);
							buffer[i][j] = NULL;
							eof_flag = 1;
							break;
						}
						if(buffer[i][j] == NULL)
							continue;
						recs_read += 1;
						read_done = 1;
						j++;
					}
					if(eof_flag == 1)
						break;
					/*if(read_done == 1)*/
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
				if(eof_flag == 1)
					break;
			}
			if(eof_flag == 1)
				continue;
			end_of_array = prev;
			buffers_used += i + 1;
			write_complete = 0;
			eof_flag = 1;
			/*break;*/	
				
		}		
		/* If buffers are full or all runs are at the end */
		else if(i == (buffers_no - 2))
		{		
			write_complete = 0;
			prev = i;
			while(write_complete == 0)
			{
				/* sort file */
				write_return = create_guilds_run(new_output, buffer, page_size, buffers_no, prev, &run_recs, &recs_write, page_count, *run_size);
				if(write_return == -1)
					return -1;
				else if(write_return == -2)
				{
						end_of_array = prev;
						buffers_used += prev + 1;
						/*printf("RECORDS READ: %d\n", run_recs);*/
						write_complete = 1;
						recs_read = 0;
						run_recs = 0;
						fseek(f_in, file_pos[prev], SEEK_SET);
						for(i = 0; i < buffers_no-1; i++)
						{
							file_pos[i] = -1;
							page_count[i] = 0;
							for(j = 0; j < page_size; j++)
								free(buffer[i][j]);
							free(buffer[i]);
						}
				}
				else
				{
					fseek(f_in, file_pos[write_return], SEEK_SET);
					j = 0;
					i = write_return;
					read_done = 0;
					while(j < page_size)
					{
						if(fread(buffer[i][j], sizeof(struct guilds), 1, f_in) != 1)
						{
							page_count[i] = (*run_size);
							buffer[i][j] = NULL;
							eof_flag = 1;
							break;
						}
						if(buffer[i][j] == NULL)
							continue;
						recs_read += 1;
						read_done = 1;
						j++;
					}
					if(eof_flag == 1)
						break;
					/*if(read_done == 1)*/
						page_count[i] += 1;
					file_pos[i] = ftell(f_in);	
				}
			}
			if(eof_flag == 1)
				continue;
			end_of_array = prev;
			buffers_used += prev + 1;
			write_complete = 0;
			i = 0;
			j = 0;
			buffer[i] = calloc(page_size, sizeof(struct guilds));
			buffer[i][j] = calloc(1, sizeof(struct guilds));
			continue;
		}
		move_places = ((*run_size)*page_size)-(page_count[i]*page_size);
		/*printf("move_places: %d\n", move_places);*/
		if(move_places > 0)
			fseek(f_in, move_places*sizeof(struct guilds), SEEK_CUR);		
		i++;
		j = 0;
		/*printf("new calloc at pos %d\n", i);
		if(i==50)
		{
			printf("at 50");
		}*/
		buffer[i] = calloc(page_size, sizeof(struct guilds));	
		if(buffer[i]==NULL){printf("calloc ERROR\n");}
		buffer[i][j] = calloc(1, sizeof(struct guilds));	
		if(buffer[i][j]==NULL){printf("calloc ERROR\n");}
	
	}
	
	*file_no += 1;
	for(k = 0; k <= end_of_array; k++)
	{
		if(page_count[k] < *run_size)
			printf("RUN %d NOT FULLY READ.\n", k);
		for(j = 0; j < page_size; j++)
		{
			free(buffer[k][j]);
		}
		free(buffer[k]);
	}

	/* CHECK IF FILE IS AT END OF FILE
 * 	IF NOT, APPEND REST OF FILE 
 * 	ALSO, OUTPUT THE CURRENT SIZE OF EACH RUN, TO CHECK IF THEY'RE DECREASING
 *
 * 	CHECK IN INPUT FUNCTION FOR IF RECORD IS NULL, SKIP IT.*/
	/*if(((buffers_no - 1) > total_runs) && eof_flag != 1)
	{
		fseek(f_in, file_pos[buffers_no - 2], SEEK_SET);
	}
	else if(((buffers_no - 1) < total_runs) && eof_flag != 1)
		fseek(f_in, file_pos[total_runs], SEEK_SET); 
	prev = ftell(f_in);
	if(!feof(f_in))
	{
		fseek(f_in, prev, SEEK_SET);
		f_out = fopen(new_output, "ab");
		if(!f_out)
		{
			fprintf(stderr, "Error opening output file for appendage.\n");
			return -1;
		}
		printf("**reading extra data**\n");
		while(fread(temp_char, sizeof(struct chars), 1, f_in) == 1)
		{
			fwrite(temp_char, sizeof(struct chars), 1, f_out);
		}
		fclose(f_out);
	}*/
	i = 0;
	f_out = fopen(new_output, "rb");
	rewind(f_out);
	/*while(fread(temp_char, sizeof(struct chars), 1, f_out) == 1)
		i++;
	printf("NUMBER OF RECORDS: %d\n", i);*/
	fclose(f_out);
	free(temp_guild);
	free(file_pos);
	free(page_count);
	fclose(f_in);
	/* Remove old temp file */
	/*remove(new_output);*/
	remove(old_output);
	printf("******* RECORDS WRITTEN: %d\n", recs_write);
	printf("Previous run size: %d\n", *run_size);
	printf("Number of buffers used: %d\n", buffers_used);	
	printf("Previous total runs: %d\n", total_runs);
	if(buffers_used > (buffers_no - 1))
		*run_size = (*run_size) * (buffers_no - 1);
	else
		*run_size = (*run_size) * buffers_used;
	if((*run_size) > (4000/page_size))
		*run_size = (int)ceil((10000.0/((double)page_size)));
	printf("New run size: %d\n", *run_size);
	printf("TEST: %.2f\n", ((double)total_runs)/((double)(buffers_no-1)));
	total_runs = (int)ceil(((double)total_runs)/((double)(buffers_no-1)));
	/*total_runs = (int)ceil(((double)total_runs)/((double)buffers_used));*/
	if(total_runs <= 0)
		total_runs = 1;
	printf("New total runs: %d\n", total_runs);	
	printf("NEW FILE: %s\n", new_output);	
	return total_runs;
}

int create_guilds_run(char * output_file, struct guilds *** buffer, int page_size, int buffers_no, int end, int * run_recs, int * recs_write, int * page_count, int run_size)
{
	FILE * f_out;
	int i, j, curr_min = 0, curr_rec = 0, records_flag = 1, early_end_flag = 0;
	f_out = fopen(output_file, "ab");
	if(!f_out)
	{
		fprintf(stderr, "Error opening new temp file.\n");
		return -1;
	}	
	
	j = 0;
	i = 0;
	/*printf("%s, %d\n", (*buffer)[0][0]->c_name, (*buffer)[0][0]->guild_id);*/
	/*printf("------------\n");*/
	while(records_flag == 1)
	{
		records_flag = 0;
		curr_rec = 0;
		curr_min = 0;
		for(i = 0; (i < buffers_no - 1 ); i++)
		{
			j = 0;
			if( i > end)
				break;
			/*if(buffer[i] == NULL)
			{
				if(page_count[i] >= run_size)
					continue;
				else
				{
					fclose(f_out);
					return i;
				}
			}*/
			/*if(buffer[50][0]==NULL)
			{
				printf("ERROR\n");
			}*/

			if(buffer[i][j] == NULL)
				continue;			

			while(strcmp(buffer[i][j]->g_name, "") == 0)
			{
				j++;
				while(buffer[i][j] == NULL && j < page_size)
				{
					j++;
				}
				if(j == page_size)
					break;
			}
			/*printf("%s, %d\n", (*buffer)[0][0]->c_name, (*buffer)[0][0]->guild_id);*/
		
			/*if(early_end_flag == 1)
			{
				early_end_flag = 0;
				continue;
			}*/
			if(j == page_size)
			{
				if(page_count[i] >= run_size)
					continue;
				else
				{
					fclose(f_out);
					return i;
				}
			}
	
			records_flag = 1;

			/* check for default min record isn't NULL */
			if(strcmp(buffer[curr_min][curr_rec]->g_name, "") == 0)
			{
				curr_min = i;
				curr_rec = j;
			}

			/* check for min record */
			else if(buffer[i][j]->guild_id < buffer[curr_min][curr_rec]->guild_id)
			{
				curr_min = i;
				curr_rec = j;
			}
		}
		if(records_flag != 0)
		{
			/*printf("WRITING: %s, %d\n", buffer[curr_min][curr_rec]->c_name, buffer[curr_min][curr_rec]->guild_id);*/
			if(fwrite(buffer[curr_min][curr_rec], sizeof(struct guilds), 1, f_out) != 1)
			{
				printf("Error writing line!\n");
				fclose(f_out);
				return -1;
			}
			/*if(curr_min==50)
			{
				printf("50");
			}*/
			memset(buffer[curr_min][curr_rec], 0, sizeof(struct guilds));
			/*if(curr_rec == page_size - 1)
				(*buffer)[curr_min] = NULL;*/
			*run_recs += 1;
			*recs_write += 1;
		}
	}
	/*printf("------------\n");*/
	fclose(f_out);
	return -2;
}
